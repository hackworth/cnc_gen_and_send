import time
from sys import stdout

from serial import Serial
from serial.tools.list_ports import comports

ADAPTER_SERIAL = 'cu.usbserial-14340'


def interp(p0, p1, pct):
    """Return the interpolated point at pct along p0 -> p1"""
    return p0 + ((p1 - p0) * pct)


def step(points, pct):
    """Compute the next version  of points. points is an array of x,y pairs"""
    n_pts = len(points)
    new_points = []
    for i in range(n_pts):
        pt0 = points[i]
        pt1 = points[(i + 1) % n_pts]
        new_points.append((interp(pt0[0], pt1[0], pct), interp(pt0[1], pt1[1], pct)))
    return new_points


def stop_cond(points, min_len):
    """Stop if length of any edge <= min_len"""
    min_len = min_len ** 2
    n_pts = len(points)
    for i in range(n_pts):
        pt0 = points[i]
        pt1 = points[(i + 1) % n_pts]
        if ((pt1[0] - pt0[0]) ** 2 + (pt1[1] - pt0[1]) ** 2) <= min_len:
            return True
    return False


def generate_gcode(points_list):
    commands = []
    indices = list(range(len(points_list[0])))
    indices.append(0)
    commands.append("G10 L20 P1 X0 Y0 Z0")
    commands.append("G10 L20 P2 X0 Y0 Z0")
    commands.append("G55")
    commands.append("F1000")
    for points in points_list:
        commands.append(f"G1 X{points[0][0]} Y{points[0][1]}")
        commands.append(f"G1 Z-0.7")
        for index in indices:
            commands.append(f"G1 X{points[index][0]} Y{points[index][1]}")
        commands.append(f"G1 Z1")
    commands.append(f"G1 X0 Y0 Z0")
    return commands


def send_command(port: Serial, command: str):
    """Sends a command to the plotter. Expects a 'ok' when done with command."""
    print(command.strip())
    port.write(command.strip().upper().encode())
    port.write(b"\n")
    # Wait until command completes
    while not port.in_waiting:
        time.sleep(0.0001)
    last_message = port.readline()
    last_message = last_message.decode("UTF-8").strip()
    return last_message == 'ok'

def get_serial_port(label):
    all_ports = comports()
    for x in all_ports:
        print(x.serial_number, x.name)
    potential_ports = [x for x in all_ports if x.serial_number == label or x.name == label]
    if 1 != len(potential_ports):
        if potential_ports:
            raise RuntimeError(f"Serial port {serial_number} multiply defined")
        else:
            raise RuntimeError(f"Can't locate serial port {serial_number}")
    port = Serial(potential_ports[0].device, baudrate=115200, timeout=120)
    time.sleep(2)  # wait for startup delay on Arduino
    return port


def main():
    x_min = 0
    x_max = 180
    y_min = 0
    y_max = 120
    min_length = 10
    offset_pct = 0.07
    draw_points = [[
        [x_min, y_min],
        (x_min, y_max),
        (x_max, y_max),
        (x_max, y_min),
    ]]
    points = step(draw_points[-1], offset_pct)

    while not stop_cond(points, min_length):
        draw_points.append(points)
        points = step(points, offset_pct)

    commands = generate_gcode(draw_points)
    with open("output.gcode", 'w') as gcode_file:
        for command in commands:
            gcode_file.write(command)
            gcode_file.write('\n')
    send_gcode(commands)


def send_gcode(commands):
    port = get_serial_port(ADAPTER_SERIAL)
    while port.in_waiting:
        stdout.write(port.read(port.in_waiting).decode("UTF-8"))
    for command in commands:
        send_command(port, command)


if '__main__' == __name__:
    main()
