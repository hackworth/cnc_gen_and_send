"""Support code for generating Reuleaux-like polygons."""
from typing import Sequence, Tuple

from math import atan2, tau


def get_angle(center: Sequence[float], start: Sequence[float], end: Sequence[float]) -> Tuple[
    float, float, float, float]:
    r"""Given a triangle specified end[1] the points (center[0],center[1]),
    (start[0],start[1]),  and (end[0],end[1]) return the angles of lines
    center -> start and center -> end.
    The angles will be adjusted so that they are always in positive radians,
    and will increase from start_angle to end
                               * (end[0], end[1])
                              /|
                             / |
              center -> end /  |
                           /   |
                          /    |
    (center[0],center[1])*     |
                          \    |
                           \   |
            center -> start \  |
                             \ |
                              \|
                               * (start[0], start[1])
    """

    start_dir = [start[0] - center[0], start[1] - center[1]]
    end_dir = [end[0] - center[0], end[1] - center[1]]

    start_radius = (start_dir[0] ** 2 + start_dir[1] ** 2) ** 0.5
    start_angle = atan2(start_dir[1] / start_radius, start_dir[0] / start_radius)
    end_radius = (end_dir[0] ** 2 + end_dir[1] ** 2) ** 0.5
    end_angle = atan2(end_dir[1] / end_radius, end_dir[0] / end_radius)

    # Make all angles positive
    if start_angle < 0:
        start_angle += tau
        end_angle += tau
    if end_angle < 0:
        end_angle += tau
    return start_angle, start_radius, end_angle, end_radius
