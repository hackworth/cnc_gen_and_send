This is a very basic GCode generator and sender. 


## Setup process:

Jog the pen to the lower left corner of the paper, with the pen tip about 0.5m above the paper. As written, the sender will set that point as the G55 coordinate system origin, and then work that system.
